package com.training.springdemo.model;

import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
@EqualsAndHashCode(of = "id")
@Entity
@Table(name="student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="student_id")
    private long id;

    @NotEmpty(message = "Student Name can not be empty!!")
    @Column(name="student_name")
    private String name;

    @NotEmpty(message = "Student email can not be empty!!")
    @Column(name="email_id")
    private String email;

    @Column(name="student_phone")
    private long phoneNo;

    public Student(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public Student(String name, String email, long phoneNo) {
        this.name = name;
        this.email = email;
        this.phoneNo = phoneNo;
    }

    public Student() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(long phoneNo) {
        this.phoneNo = phoneNo;
    }
}
