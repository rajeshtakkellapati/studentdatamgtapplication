package com.training.springdemo.repository;

import com.training.springdemo.model.Student;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import org.hibernate.query.Query;
import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class StudentDAOImpl implements StudentDAO{
    private EntityManager entityManager;

    @Autowired
    public StudentDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Student> findAll() {
        Session currentSession = entityManager.unwrap(Session.class);

        Query<Student> theQuery = currentSession.createQuery("from Student", Student.class);

        List<Student> students = theQuery.getResultList();

        return students;
    }

    @Override
    public Student getStudent(long id) {
        Session currentSession = entityManager.unwrap(Session.class);
        Student student = currentSession.get(Student.class, id);
        return student;
    }

    @Override
    public Student saveStudent(Student student) {
        Session currentSession = entityManager.unwrap(Session.class);
        currentSession.saveOrUpdate(student);
        return student;
    }

    @Override
    public void removeStudent(long id) {
        Session currentSession = entityManager.unwrap(Session.class);
        Query theQuery = currentSession.createQuery("delete from Student where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
    }
}
