package com.training.springdemo.service;

import com.training.springdemo.model.Student;

import java.util.List;

public interface StudentService {
    void addStudent(Student student);

    List<Student> getStudentData();

    void removeStudent(long id);

    void updateStudent(Student student);

    Student getStudent(long id);
}
