package com.training.springdemo.service;

import com.training.springdemo.model.Student;
import com.training.springdemo.model.StudentData;
import com.training.springdemo.repository.StudentDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService{

    //private final StudentData data = new StudentData();

    private StudentDAO studentDAO;

    @Autowired
    public StudentServiceImpl(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    @Override
    @Transactional
    public void addStudent(Student student) {
        student.setId(0L);
        studentDAO.saveStudent(student);
    }

    @Override
    @Transactional
    public List<Student> getStudentData() {
        return studentDAO.findAll();
    }

    @Override
    @Transactional
    public void removeStudent(long id) {
        studentDAO.removeStudent(id);
    }

    @Override
    @Transactional
    public void updateStudent(Student student) {
        studentDAO.saveStudent(student);
    }

    @Override
    @Transactional
    public Student getStudent(long id) {
        return studentDAO.getStudent(id);
    }
}
