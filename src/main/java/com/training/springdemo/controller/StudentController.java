package com.training.springdemo.controller;

import com.training.springdemo.model.Student;
import com.training.springdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/students/")
public class StudentController {
    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("list")
    public String showStudentList(Model model) {
        model.addAttribute("students", studentService.getStudentData());
        return "index";
    }

    @GetMapping("signup")
    public String showSignUpForm(Student student) {
        return "add-student";
    }

    @PostMapping("add")
    public String addStudent(@Valid Student student, BindingResult result, Model model){
        if(result.hasErrors()) {
            return "add-student";
        }
        studentService.addStudent(student);
        return "redirect:list";
    }

    @GetMapping("edit/{idValue}")
    public String showUpdateForm(@PathVariable("idValue") long id, Model model) {
        Student student = studentService.getStudent(id);
        model.addAttribute("student", student);
        return "update-student";
    }

    @PostMapping("update/{id}")
    public String updateStudent(@PathVariable("id") long id,@ModelAttribute("student") Student student, Model model) {
        studentService.updateStudent(student);
        model.addAttribute("students", studentService.getStudentData());
        return "index";
    }

    @GetMapping("delete/{id}")
    public String deleteStudent(@PathVariable("id") long id, Model model) {
        studentService.removeStudent(id);
        model.addAttribute("students", studentService.getStudentData());
        return "index";
    }
}
